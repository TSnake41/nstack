/*
    Copyright 2017 Astie Teddy

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdlib.h>
#include <stdbool.h>

typedef struct nstack {
    size_t size, count;
    void *v[];
} nstack;

#define NSTACK_REALLOC(stack, new_size) \
    realloc((stack), sizeof(nstack) + sizeof(void *) * (new_size))

nstack *nstack_new(void)
{
    nstack *new_stack = malloc(sizeof(nstack) + sizeof(void *));
    if (new_stack == NULL)
        return NULL;

    new_stack->size = 1;
    new_stack->count = 0;

    return new_stack;
}

void nstack_free(nstack *stack)
{
    if (stack == NULL)
        return;

    free(stack);
}

bool nstack_push(nstack **stack_ptr, void *value)
{
    if (stack_ptr == NULL)
        return true;

    nstack *stack = *stack_ptr;

    if (stack == NULL)
        return true;

    if (stack->size == stack->count) {
        if (stack->size == 0)
            stack->size = 1;

        nstack *new_stack = NSTACK_REALLOC(stack, stack->size * 2);
        if (new_stack == NULL)
            return true;

        new_stack->size *= 2;
        *stack_ptr = stack = new_stack;
    }

    stack->v[stack->count++] = value;
    return false;
}

void *nstack_pull(nstack **stack_ptr)
{
    if (stack_ptr == NULL)
        return NULL;

    nstack *stack = *stack_ptr;

    if (stack == NULL)
        return NULL;

    if (stack->count == 0)
        return NULL;

    void *value = stack->v[--stack->count];

    if (stack->count == (stack->size / 2)) {
        nstack *new_stack = NSTACK_REALLOC(stack, stack->size / 2);
        if (new_stack != NULL) {
            new_stack->size /= 2;
            *stack_ptr = new_stack;
        }
    }

    return value;
}

size_t nstack_count(nstack *stack)
{
    if (stack == NULL)
        return 0;

    return stack->count;
}
